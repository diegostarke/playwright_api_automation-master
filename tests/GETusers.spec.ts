import { test, expect } from "@playwright/test";

test.describe("usuarios/", async () => {
  test("GET Lista usuarios cadastrados", async ({ request }) => {
    const response = await request.get("usuarios/");

    expect(response.status()).toBe(200);
    const body = await response.json();
    expect(body.usuarios.length).toBeGreaterThanOrEqual(1);
  });

  test("GET Lista usuarios com id específico", async ({ request }) => {
    const responseList = await request.get("usuarios/");
    const bodyList = await responseList.json();

    const response = await request.get(`usuarios/${bodyList.usuarios[0]._id}`);

    expect(response.status()).toBe(200);
    expect(await response.json()).toEqual(expect.objectContaining({
      nome: bodyList.usuarios[0].nome,
      email: bodyList.usuarios[0].email,
      password: bodyList.usuarios[0].password,
      administrador: bodyList.usuarios[0].administrador,
      _id: bodyList.usuarios[0]._id,
    }))
  });
});