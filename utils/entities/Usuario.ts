import { faker } from '@faker-js/faker';

type Administrator = 'true' | 'false';

interface Usuario {
    nome: string;
    email: string;
    password: string;
    administrator: Administrator;
}

export function createRandomUser(): Usuario {
    return {
        nome: faker.person.fullName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
        administrator: faker.helpers.arrayElement(['true', 'false']),
    };
  }
  
  const user = createRandomUser();