***Projeto de Testes de API com Playwright***
Este é um projeto para testes de API utilizando o Playwright. 
O objetivo deste projeto é executar testes automatizados para APIs.

Pré-requisitos
Antes de começar, você precisa ter o seguinte instalado na sua máquina:

Node.js (versão 14 ou superior)
npm (geralmente instalado junto com o Node.js)
faker

Instalação
Clone este repositório
Instale as dependências do projeto

Estrutura do Projeto
A estrutura básica do projeto é a seguinte:
tests/GETuser.spec.ts: Contém os arquivos de testes de API com método GET
tests/POSTlogin.spec.ts: Contém os arquivos de testes de API com método POST
playwright.config.js: Arquivo de configuração do Playwright.
package.json: Contém as dependências e scripts do projeto.

Configuração do Playwright
O arquivo playwright.config.js é onde você pode configurar o Playwright para o seu projeto.

Executando os Testes
Para executar os testes, utilize o seguinte comando:npx playwright test
Este comando irá procurar pelos arquivos de teste na pasta tests/api e executá-los.
